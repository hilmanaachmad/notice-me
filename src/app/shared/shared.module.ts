import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MenuItems } from './menu-items/menu-items';
import { ToggleFullscreenDirective } from './fullscreen/toggle-fullscreen.directive';
import { CardRefreshDirective } from './card/card-refresh.directive';
import { CardToggleDirective } from './card/card-toggle.directive';
import { CardComponent } from './card/card.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ParentRemoveDirective } from './elements/parent-remove.directive';
// import {SqueezeBoxModule} from 'squeezebox';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SpinnerComponent } from '../spinner/spinner.component';
import { ModalAnimationComponent } from './modal-animation/modal-animation.component';
import { ModalBasicComponent } from './modal-basic/modal-basic.component';
import { SelectOptionService } from './elements/select-option.service';
import { DataFilterPipe } from './elements/data-filter.pipe';

import { TodoService } from './todo/todo.service';
import { HorizontalTimelineModule } from "./horizontal-timeline/horizontal-timeline.module";

import {DataTableModule} from "angular-6-datatable";
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule.forRoot(),
        HorizontalTimelineModule,
        DataTableModule,
        NgSelectModule
    ],
    declarations: [
        ToggleFullscreenDirective,
        CardRefreshDirective,
        CardToggleDirective,
        ParentRemoveDirective,
        CardComponent,
        SpinnerComponent,
        ModalAnimationComponent,
        ModalBasicComponent,
        DataFilterPipe
    ],
    exports: [
        ToggleFullscreenDirective,
        CardRefreshDirective,
        CardToggleDirective,
        ParentRemoveDirective,
        CardComponent,
        SpinnerComponent,
        NgbModule,
        FormsModule,
        ReactiveFormsModule,
        ModalBasicComponent,
        ModalAnimationComponent,
        DataFilterPipe,
        HorizontalTimelineModule,
        DataTableModule,
        NgSelectModule
    ],
    providers: [
        MenuItems,
        TodoService,
        SelectOptionService
    ]
})
export class SharedModule { }
