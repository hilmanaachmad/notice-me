import { Routes } from '@angular/router';
import { AdminLayoutComponent } from './layouts/admin/admin-layout.component';
import { UserLayoutComponent } from './layouts/user/user-layout.component';
import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';
import { AuthGuard } from './auth.guard';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

export const AppRoutes: Routes = [
    {
        path: '',
        redirectTo: 'auth/login',
        pathMatch: 'full'
    },
    {
        path: '',
        component: AuthLayoutComponent,
        children: [
            {
                path: 'auth',
                loadChildren: './authentication/authentication.module#AuthenticationModule'
            }]
    },
    {
        path: 'user',
        component: UserLayoutComponent,
        children: [
            {
                path: 'home',
                loadChildren: './user/dashboard/admin-dashboard.module#AdminDashboardModule'
            },
            {
                path: 'email',
                loadChildren: './user/email/email.module#UserEmailModule'
            },
            {
                path: 'telegram',
                loadChildren: './user/telegram/telegram.module#UserTelegramModule'
            },
        ],
        canActivate: [AuthGuard]
    },
    {
        path: 'admin',
        component: AdminLayoutComponent,
        children: [
            {
                path: 'home',
                loadChildren: './admin/admin-home/admin-home.module#AdminHomeModule'
            },
            {
                path: 'user',
                loadChildren: './admin/user/user.module#UserModule'
            },
            {
                path: 'email',
                loadChildren: './admin/admin-email/admin-email.module#AdminEmailModule'
            },
            {
                path: 'telegram',
                loadChildren: './admin/admin-telegram/admin-telegram.module#AdminTelegramModule'
            }
        ]
    },
    {
        path: '**',
        component: PageNotFoundComponent
    }
];
