import { Component } from '@angular/core';
import { SessionService } from 'src/app/shared/service/session.service';
import { Title } from '@angular/platform-browser';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'admin-nav',
  templateUrl: './admin-nav.component.html',
})
export class AdminNavComponent {
  constructor(
    private router: Router,
    public session: SessionService
  ) {
  }
  logout() {
    this.session.logOut()
    this.router.navigate(['/auth/admin'])
  }
}
