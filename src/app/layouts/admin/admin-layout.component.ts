import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AppService } from '../../shared/service/app.service';
import { SessionService } from 'src/app/shared/service/session.service';
import { Router } from '@angular/router';

declare var $: any;
declare var jQuery: any

@Component({
  selector: 'app-admin-layout',
  templateUrl: './admin-layout.component.html',
  styleUrls: ['./admin-layout.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class AdminLayoutComponent implements OnInit {
  public userInfo: any;
  public tanggal: any;
  constructor(
    private service: AppService,
    private router: Router,
    public session: SessionService
  ) {
    (function ($) {
      'use strict';
      $(function () {
        var body = $('body');
        var contentWrapper = $('.content-wrapper');
        var scroller = $('.container-scroller');
        var footer = $('.footer');
        var sidebar = $('.sidebar');
        var navbar = $('.navbar').not('.top-navbar');

        function addActiveClass(element) {
          if (current === "") {
            //for root url
            if (element.attr('href').indexOf("index.html") !== -1) {
              element.parents('.nav-item').last().addClass('active');
              if (element.parents('.sub-menu').length) {
                element.closest('.collapse').addClass('show');
                element.addClass('active');
              }
            }
          } else {
            //for other url
            if (element.attr('href').indexOf(current) !== -1) {
              element.parents('.nav-item').last().addClass('active');
              if (element.parents('.sub-menu').length) {
                element.closest('.collapse').addClass('show');
                element.addClass('active');
              }
              if (element.parents('.submenu-item').length) {
                element.addClass('active');
              }
            }
          }
        }

        var current = location.pathname.split("/").slice(-1)[0].replace(/^\/|\/$/g, '');
        $('.nav li a', sidebar).each(function () {
          var $this = $(this);
          addActiveClass($this);
        })

        $('.horizontal-menu .nav li a').each(function () {
          var $this = $(this);
          addActiveClass($this);
        })

        //Close other submenu in sidebar on opening any

        sidebar.on('show.bs.collapse', '.collapse', function () {
          sidebar.find('.collapse.show').collapse('hide');
        });

        $('[data-toggle="minimize"]').on("click", function () {
          if (body.hasClass('sidebar-toggle-display')) {
            body.toggleClass('sidebar-hidden');
          } else {
            body.toggleClass('sidebar-icon-only');
          }
        });

        //checkbox and radios
        $(".form-check label,.form-radio label").append('<i class="input-helper"></i>');

        //Horizontal menu in mobile
        $('[data-toggle="horizontal-menu-toggle"]').on("click", function () {
          $(".horizontal-menu .bottom-navbar").toggleClass("header-toggled");
        });
        // Horizontal menu navigation in mobile menu on click
        var navItemClicked = $('.horizontal-menu .page-navigation >.nav-item');
        navItemClicked.on("click", function (event) {
          if (window.matchMedia('(max-width: 991px)').matches) {
            if (!($(this).hasClass('show-submenu'))) {
              navItemClicked.removeClass('show-submenu');
            }
            $(this).toggleClass('show-submenu');
          }
        })

        $(window).scroll(function () {
          if (window.matchMedia('(min-width: 992px)').matches) {
            var header = $('.horizontal-menu');
            if ($(window).scrollTop() >= 150) {
              $(header).addClass('fixed-on-scroll');
            } else {
              $(header).removeClass('fixed-on-scroll');
            }
          }
        });

        // fixed navbar on scroll
        $(window).scroll(function () {
          if (window.matchMedia('(min-width: 992px)').matches) {
            if ($(window).scrollTop() >= 197) {
              $(navbar).addClass('navbar-mini fixed-top');
              $(body).addClass('navbar-fixed-top');
            } else {
              $(navbar).removeClass('navbar-mini fixed-top');
              $(body).removeClass('navbar-fixed-top');
            }
          }
        });

        if (window.matchMedia('(max-width: 991px)').matches) {
          $(navbar).addClass('navbar-mini fixed-top');
          $(body).addClass('navbar-fixed-top');
        }

        function toggleNavbarMini() {
          if (window.matchMedia('(max-width: 991px)').matches) {
            $(navbar).addClass('navbar-mini');
          } else {
            $(navbar).removeClass('navbar-mini');
          }
        }

        window.addEventListener("resize", toggleNavbarMini);
        toggleNavbarMini();


      });
    })(jQuery);

    (function ($) {
      'use strict';
      $(function () {
        $('[data-toggle="offcanvas"]').on("click", function () {
          $('.sidebar-offcanvas').toggleClass('active')
        });
      });
    })(jQuery);

  }

  ngOnInit() {
    if (this.service.getRole() !== 'admin'){
      this.router.navigate(['/user/home'])
    }

    setTimeout(() => {
      this.service.checkToken()
    }, 500);

    this.userInfo = {
      name: this.service.getNama(),
      role: this.service.getRole()

    };

    setInterval(() => {
      this.tanggal = this.service.HariIni();
    }, 1000);


  }



  logout() {
    this.session.logOut()
    this.router.navigate(['/auth/admin'])
  }
}
