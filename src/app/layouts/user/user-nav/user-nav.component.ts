import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { SessionService } from 'src/app/shared/service/session.service';

@Component({
  selector: 'user-nav',
  templateUrl: './user-nav.component.html',
})
export class UserNavComponent { 
  constructor(
    private router: Router,
    public session: SessionService
  ) {
  }
  logout() {
    this.session.logOut()
    this.router.navigate(['/auth/login'])
  }
}
