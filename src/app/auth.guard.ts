import { Injectable } from '@angular/core';
import { CanActivate,  Router } from '@angular/router';
import { AppService } from './shared/service/app.service';


@Injectable()
export class AuthGuard implements CanActivate {
  
  constructor(
    public service: AppService,
    private router: Router) { }

  canActivate(): boolean{
    if (this.service.loggedIn()){
      return true
    } else {
      this.router.navigate([''])
      return false
    }
  }
}
