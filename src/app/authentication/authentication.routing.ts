import { Routes } from '@angular/router';

export const AuthenticationRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'login',
        loadChildren: './login/login.module#LoginModule',
        data: {
          breadcrumb: 'Login User'
        }
      },
      {
        path: 'admin',
        loadChildren: './admin-login/admin-login.module#AdminLoginModule',
        data: {
          breadcrumb: 'Login Admin'
        }
      }
    ]
  }
];


