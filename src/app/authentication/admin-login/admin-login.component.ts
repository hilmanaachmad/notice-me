import { Component, OnInit } from '@angular/core';
import { AppService } from '../../shared/service/app.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/shared/service/session.service';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.css']
})
export class AdminLoginComponent implements OnInit {
  imgBg = 'assets/pocari.jpg';
  loginForm: FormGroup;
  submitted = false;
  data : any;
  loginUserData = {};

  constructor(
    public service: AppService,
    public session: SessionService,
    private formBuilder: FormBuilder,
    private _auth: AppService,
    private _router: Router) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }
  get f() { return this.loginForm.controls; }


  loginAdmin() {
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    } 

    this.service.post('/api/admin/login', this.loginForm.value)
      .subscribe( 
        data => {
          var result = JSON.parse(data)
          if (result.status == true) {
            this.session.setSession('id', result.data.id)
            this.session.setSession('nik', result.data.nik)
            this.session.setSession('name', result.data.name)
            this.session.setSession('role', result.data.role)
            this.session.setSession('token', result.data.token)
            this.session.setSession('username', result.data.username)
            this.session.setSession('email', result.data.email)
            this._router.navigate(['/admin/home']).then(() => {
              window.location.reload();
            });
          } else {
            this.service.openErrorSwal('Username & Password salah / tidak ada')
          }
        },
        err => console.log(err)
      )
  }

  onReset() {
    this.submitted = false;
    this.loginForm.reset();
  }

}
