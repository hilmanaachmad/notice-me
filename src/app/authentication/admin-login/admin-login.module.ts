import { NgModule } from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import { CommonModule } from '@angular/common';
import { AdminLoginComponent } from './admin-login.component';
import {SharedModule} from "../../shared/shared.module";
import { ReactiveFormsModule } from '@angular/forms';

export const AdminRoutes: Routes = [
  {
    path: '',
    component: AdminLoginComponent,
    data: {
      breadcrumb: 'Login Admin',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  declarations: [AdminLoginComponent],
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
    RouterModule.forChild(AdminRoutes)
  ]
})
export class AdminLoginModule { }
