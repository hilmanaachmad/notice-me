import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserTelegramRoutes } from './telegram.routing';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(UserTelegramRoutes),
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: []
})

export class UserTelegramModule {}
