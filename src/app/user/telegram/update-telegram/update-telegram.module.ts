import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes} from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';
import { UserUpdateTelegramComponent } from './update-telegram.component';
export const UserUpdateTelegramRoutes: Routes = [
  {
    path: '',
    component: UserUpdateTelegramComponent,
    data: {
      breadcrumb: 'Update Telegram',
      status: true
    }
  }
];

@NgModule({
  declarations: [UserUpdateTelegramComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(UserUpdateTelegramRoutes),
    SharedModule,
    NgxLoadingModule
  ]
})
export class UserUpdateTelegramModule { }
