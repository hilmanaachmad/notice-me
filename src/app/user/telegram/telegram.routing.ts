import { Routes } from '@angular/router';

export const UserTelegramRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'add-telegram',
        loadChildren: './add-telegram/add-telegram.module#UserAddTelegramModule',
        data: {
          breadcrumb: 'Add telegram'
        }
      },
      {
        path: 'list-telegram',
        loadChildren: './list-telegram/list-telegram.module#UserListTelegramModule',
        data: {
          breadcrumb: 'List User'
        }
      },
      {
        path: 'update-telegram',
        loadChildren: './update-telegram/update-telegram.module#UserUpdateTelegramModule',
        data: {
          breadcrumb: 'Update telegram'
        }
      }
    ]
  }
];


