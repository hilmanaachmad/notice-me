import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes} from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';
import { UserAddTelegramComponent } from './add-telegram.component';
export const UserAddTelegramRoutes: Routes = [
  {
    path: '',
    component: UserAddTelegramComponent,
    data: {
      breadcrumb: 'Add Telegram',
      status: true
    }
  }
];
@NgModule({
  declarations: [UserAddTelegramComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(UserAddTelegramRoutes),
    SharedModule,
    NgxLoadingModule
  ]
})
export class UserAddTelegramModule { }
 