import { Component, OnInit, Renderer, Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { AppService } from '../../../shared/service/app.service';
import swal from 'sweetalert2';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-telegram',
  templateUrl: './list-telegram.component.html',
  styleUrls: ['./list-telegram.component.css'],
  providers: [DatePipe]
})
export class UserListTelegramComponent implements OnInit {
  public nikPengirim: any;
  public data: any;
  public pesan: any;
  public rowsOnPage: number = 10;
  public filterQuery: string = "";
  public sortBy: string = "Name";
  public sortOrder: string = "asc";
  public loading = false;
  public test = 'robi';
  private myDate = new Date();
 
  constructor(
    private router: Router,
    public datepipe: DatePipe,
    private http: HttpClient, 
    private service: AppService,
     private renderer: Renderer) {   }

  ngOnInit() {
    this.nikPengirim = this.service.getNik();
    this.loading = true;
    let data = ''
    this.service.get('/api/noticeme/master-data/telegram/get' + '/' + this.nikPengirim, data)
      .subscribe(result => {
        this.loading = false;
        let valueResult = JSON.parse(result)
        if (valueResult.status == true) {
          this.data = valueResult.data
        } else {
          this.data = ''
        }
      }, error => {
        this.loading = false;
        swal.fire(
          'Authentication!',
          'Can\'t connect to server.',
          'error'
        );
      });
  }
 
  deleteTelegram(id) {
    let data = ''
    swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      reverseButtons: true
    }).then(result => {
      if (result.value == true) {
        this.loading = true;
        this.service.delete('/api/noticeme/master-data/telegram/delete' + '/' + id, data)
          .subscribe(res => {
            this.loading = false;
            swal.fire("Good job!", "Transaction Deleted Successfully!", "success");
            this.ngOnInit();
          })
      }
    })
  }

  updateTelegram(id){
    this.router.navigate(['user/telegram/update-telegram'],{
      queryParams:{
        id: btoa(id)
      }
    })
  }
 
  lihatPesan(id){
    let pesan = ''
    this.service.get('/api/noticeme/master-data/telegram/pesan' + '/' + id , pesan)
      .subscribe(result => {
        this.loading = false;
        let valueResult = JSON.parse(result)
        if (valueResult.status == true) {
          this.pesan = valueResult.data
          console.log(this.pesan);
        } else {
          this.pesan = ''
        }
      }, error => {
        this.loading = false;
        swal.fire(
          'Authentication!',
          'Can\'t connect to server.',
          'error'
        );
      });
  }
  onActived(id, isActived){
    let data = ''
    if (isActived == 0) {
      isActived = 1;
    } else {
      isActived = 0;
    }
    swal.fire({
      title: 'Are you sure to change?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, change it!',
      reverseButtons: true
    }).then(result => {
      if (result.value == true) {
        this.loading = true;
        this.service.put('/api/noticeme/master-data/telegram/actived' + '/' + isActived + '/' + id, data)
          .subscribe(res => {
            this.loading = false;
            swal.fire("Good job!", "Transaction Changed Successfully!", "success");
            this.ngOnInit();
          })
      }
    })

    
    // alert('id yang dipilih '+ id + ' nilai balik :' +isApproved);

  }
  onClick(id, isApproved){
    let data = ''
    if (isApproved == 0) {
      isApproved = 1;
    } else {
      isApproved = 0;
    }
    swal.fire({
      title: 'Are you sure to change?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, change it!',
      reverseButtons: true
    }).then(result => {
      if (result.value == true) {
        this.loading = true;
        this.service.put('/api/noticeme/master-data/telegram/approved' + '/' + isApproved + '/' + id, data)
          .subscribe(res => {
            this.loading = false;
            swal.fire("Good job!", "Transaction Changed Successfully!", "success");
            this.ngOnInit();
          })
      }
    })

    
    // alert('id yang dipilih '+ id + ' nilai balik :' +isApproved);
}
}