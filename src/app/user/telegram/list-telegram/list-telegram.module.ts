import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import { UserListTelegramComponent } from './list-telegram.component';
import {FilterTablePipe} from './list-telegram.pipe';
import {SharedModule} from '../../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

export const UserListTelegramRoutes: Routes = [
  {
    path: '',
    component: UserListTelegramComponent,
    data: {
      breadcrumb: 'List Telegram',
      status: true
    }
  }
];
@NgModule({
  declarations: [UserListTelegramComponent, FilterTablePipe],
  imports: [
    CommonModule,
    RouterModule.forChild(UserListTelegramRoutes),
    SharedModule,
    NgxLoadingModule
  ]
})
export class UserListTelegramModule { }