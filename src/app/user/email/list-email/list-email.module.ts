import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes} from '@angular/router';
import { UserListEmailComponent } from './list-email.component';
import {FilterTablePipe} from './list-email.pipe'
import { SharedModule } from '../../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';
export const UserListEmailRoutes: Routes = [
  {
    path: '',
    component: UserListEmailComponent,
    data: {
      breadcrumb: 'List Email',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(UserListEmailRoutes),
    SharedModule,
    NgxLoadingModule
  ],
  declarations: [UserListEmailComponent, FilterTablePipe]
})
export class UserListEmailModule { }
