import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes} from '@angular/router';
import { UpdateEmailComponent } from './update-email.component';
import { SharedModule } from '../../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';
import { EditorModule } from '@tinymce/tinymce-angular';
import { ReactiveFormsModule } from '@angular/forms';

export const UpdateEmailRoutes: Routes = [
  {
    path: '',
    component: UpdateEmailComponent,
    data: {
      breadcrumb: 'Update Email',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(UpdateEmailRoutes),
    EditorModule,
    ReactiveFormsModule,
    SharedModule,
    NgxLoadingModule
  ],
  declarations: [UpdateEmailComponent]
})
export class UserUpdateEmailModule { }
