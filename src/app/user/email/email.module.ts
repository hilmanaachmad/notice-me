// import { NgModule } from '@angular/core';
// import { CommonModule } from '@angular/common';
// import { EmailComponent } from './email.component';
// import {RouterModule, Routes} from "@angular/router";
// import {SharedModule} from "../../shared/shared.module";
// import { AddEmailComponent } from './add-email/add-email.component';

// export const EmailRoutes: Routes = [
//   {
//     path: '',
//     component: EmailComponent,
//     data: {
//       breadcrumb: 'Dashboard Admin',
//       icon: 'icofont-home bg-c-blue',
//       status: false
//     }
//   }
// ];

// @NgModule({
//   imports: [
//     CommonModule,
//     RouterModule.forChild(EmailRoutes),
//     SharedModule
//   ],
//   declarations: [EmailComponent, AddEmailComponent]
// })
// export class EmailModule { }

import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserEmailRoutes } from './email.routing';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(UserEmailRoutes),
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: []
})

export class UserEmailModule {}
