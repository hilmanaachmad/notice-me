import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes} from '@angular/router';
import { UserAddEmailComponent } from './add-email.component';
import { SharedModule } from '../../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';
import { EditorModule } from '@tinymce/tinymce-angular';
import { ReactiveFormsModule } from '@angular/forms';

export const UserAddEmailRoutes: Routes = [
  {
    path: '',
    component: UserAddEmailComponent,
    data: {
      breadcrumb: 'Add Email user',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(UserAddEmailRoutes),
    EditorModule,
    ReactiveFormsModule,
    SharedModule,
    NgxLoadingModule
  ],
  declarations: [UserAddEmailComponent]
})
export class UserAddEmailModule { }
