import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Http, Headers, Response } from "@angular/http";
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { DatePipe } from '@angular/common';

import { AppService } from '../../../shared/service/app.service';
import { SessionService } from '../../../shared/service/session.service';

// declare var tinymce: any;

@Component({
  selector: 'app-add-email',
  templateUrl: './add-email.component.html',
  styleUrls: ['./add-email.component.css'],
  providers: [DatePipe]
})
export class UserAddEmailComponent implements OnInit {
  // isi = new FormControl('');
  public options = [{ id: 1, label: 'Active' }, { id: 0, label: 'Not Active' }];
  public categories = [{ id: 'bydate', label: 'By date every month' }, { id: 'bybetween', label: 'By time period' }];
  tanggals = ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31'];
  public emailInfo : any;
  public tanggal : any;
  private myDate = new Date();
  myForm: FormGroup;
  public loading = false;
  data_linesection = []
  datalevel = [
    {
      id: 'SUPERADMIN', name: 'SUPERADMIN'
    }
  ]

  constructor(
    private router: Router,
    private _http: Http,
    private http: HttpClient,
    private service: AppService,
    public session: SessionService,
    private datePipe: DatePipe) {

    let kategori = new FormControl(null, Validators.required);
    let emailpengirim = new FormControl(this.service.getEmail(), Validators.required);
    let emailpenerima = new FormControl('', Validators.required);
    let subject = new FormControl('', Validators.required);
    let pesan = new FormControl('', Validators.required);
    let start_date = new FormControl('', Validators.required);
    let end_date = new FormControl('', Validators.required);
    let by_date = new FormControl('', Validators.required);
    let isActived = new FormControl(null, Validators.required)

    this.myForm = new FormGroup({
      kategori: kategori,
      emailpengirim: emailpengirim,
      emailpenerima: emailpenerima,
      subject: subject,
      pesan: pesan,
      start_date: start_date,
      end_date: end_date,
      by_date: by_date,
      isActived: isActived
    });


  }

  setKategoriValidators() {
    const by_date = this.myForm.get('by_date');
    const start_date = this.myForm.get('start_date');
    const end_date = this.myForm.get('end_date');

    this.myForm.get('kategori').valueChanges
      .subscribe(userCategory => {

        if (userCategory === 'bydate') {
          by_date.setValidators([Validators.required]);
          start_date.setValidators(null);
          end_date.setValidators(null);
        }

        if (userCategory === 'bybetween') {
          by_date.setValidators(null);
          start_date.setValidators([Validators.required]);
          end_date.setValidators([Validators.required]);
        }

        by_date.updateValueAndValidity();
        start_date.updateValueAndValidity();
        end_date.updateValueAndValidity();
      });
  }


  ngOnInit() {
  	this.emailInfo = {
      name: this.service.getNama(),
      email: this.service.getEmail()
    }
    this.setKategoriValidators();
    // this.tanggal = this.service.HariIni();
  		// tinymce.init(
    //     {
    //         selector: "#mymce1"
    //     });
  }

  submitted: boolean
  onSubmit() {
    this.submitted = true;
    if (this.myForm.valid) {
      let dataBody = this.myForm.value
      swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, add it!',
        reverseButtons: true
      }).then(result => {
        if (result.value == true) {
          this.loading = true;
          this.service.post('/api/noticeme/master-data/email/add', dataBody)
            .subscribe(data => {
              this.loading = false;
              let result = JSON.parse(data)
              if (result.status == true) {
                swal.fire("Good job!", "Add Transaksi Email Success!", "success");
                this.myForm.reset()
              } else {
                swal.fire(
                  'Authentication!',
                  'Failed Insert.',
                  'error'
                );
              }
            }, error => {
              this.loading = false;
              swal.fire(
                'Authentication!',
                'Can\'t connect to server.',
                'error'
              );
            });
        }
      })
    }
  }
}