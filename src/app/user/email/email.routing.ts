import { Routes } from '@angular/router';

export const UserEmailRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'add-email',
        loadChildren: './add-email/add-email.module#UserAddEmailModule',
        data: {
          breadcrumb: 'Add Email'
        }
      },
      {
        path: 'list-email',
        loadChildren: './list-email/list-email.module#UserListEmailModule',
        data: {
          breadcrumb: 'List Email'
        }
      },
      {
        path: 'update-email',
        loadChildren: './update-email/update-email.module#UserUpdateEmailModule',
        data: {
          breadcrumb : 'Update Email'
        }
      }
    ]
  }
];


