import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { AdminLayoutComponent } from './layouts/admin/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';
import { SharedModule } from './shared/shared.module';
import { LocationStrategy, PathLocationStrategy, DatePipe, HashLocationStrategy } from '@angular/common';
import { AppService } from './shared/service/app.service';
import { AuthGuard } from './auth.guard';
import { SessionService } from './shared/service/session.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutes } from './app-routing.module';
import { TokenInterceptorService } from './token-interceptor.service';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { NgxLoadingModule } from 'ngx-loading';
import { AdminNavComponent } from './layouts/admin/admin-nav/admin-nav.component';

import { UserNavComponent } from './layouts/user/user-nav/user-nav.component';
import { UserLayoutComponent } from './layouts/user/user-layout.component';
@NgModule({
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    AuthLayoutComponent,
    PageNotFoundComponent,
    AdminNavComponent,
    UserNavComponent,
    UserLayoutComponent

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    SharedModule,
    RouterModule.forRoot(AppRoutes),
    FormsModule,
    // tslint:disable-next-line: deprecation
    HttpModule,
    HttpClientModule,
    NgxLoadingModule
  ],
  providers: [
    AppService,
    AuthGuard,
    SessionService,
    DatePipe,
    { provide: LocationStrategy, useClass: PathLocationStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
