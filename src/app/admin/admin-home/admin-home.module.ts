import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminHomeComponent } from './admin-home.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";
import { FullCalendarModule } from '@fullcalendar/angular';

export const AdminHomeRoutes: Routes = [
  {
    path: '',
    component: AdminHomeComponent,
    data: {
      breadcrumb: 'Dashboard Home',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminHomeRoutes),
    SharedModule,
    FullCalendarModule
  ],
  declarations: [AdminHomeComponent]
})
export class AdminHomeModule { }
