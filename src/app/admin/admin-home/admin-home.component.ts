import { Component, OnInit } from '@angular/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { AppService } from '../../shared/service/app.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-admin-home',
  templateUrl: './admin-home.component.html',
  styleUrls: ['./admin-home.component.css']
})
export class AdminHomeComponent implements OnInit {
  public nikPengirim: any;
  public data: any;
  public loading = false;
  public transaksi_email : any;
  public transaksi_telegram : any;

	calendarEvents:any[]=[
		{
      		title: 'Between',
            start: '2020-02-05',
            end: '2020-02-10',
            color: '#00BFFF'
        },
    	{ 
    		title: 'Date', 
    		date: '2020-03-05',
    		color: '#32CD32'
    	}
	]
	calendarPlugins=[dayGridPlugin]
	calendarHeader={
      left: 'prev,next today',
      center: 'title',
      right: 'dayGridMonth,dayGridWeek,dayGridDay,ListWeek'
    }

	calendarOptions = {
    plugins: [dayGridPlugin],
    header: {
      left: 'prev,next,today',
      center: 'title',
      right: 'dayGridMonth,dayGridWeek,dayGridDay'
    },
    buttonText: {
      today:    'Today',
      month:    'Month',
      week:     'Week',
      day:      'Days'
    },
    defaultView: 'dayGridMonth'
  }

  constructor(
    private http: HttpClient,
    private service: AppService
  ) { }

  ngOnInit() {

    let transaksi_email = ''
    this.service.get('/api/noticeme/master-data/email/totalTransaksiEmailForAdmin' , transaksi_email)
      .subscribe(result => {
        this.loading = false;
        let valueResult = JSON.parse(result)
        if (valueResult.status == true) {
          this.transaksi_email = valueResult.data
          console.log(this.transaksi_email);
        } else {
          this.transaksi_email = ''
        }
      }, error => {
        this.loading = false;
        swal.fire(
          'Authentication!',
          'Can\'t connect to server.',
          'error'
        );
      });

      let transaksi_telegram = ''
      this.service.get('/api/noticeme/master-data/telegram/totalTransaksiTelegramForAdmin' , transaksi_telegram)
        .subscribe(result => {
          this.loading = false;
          let valueResult = JSON.parse(result)
          if (valueResult.status == true) {
            this.transaksi_telegram = valueResult.data
            console.log(this.transaksi_telegram);
          } else {
            this.transaksi_telegram = ''
          }
        }, error => {
          this.loading = false;
          swal.fire(
            'Authentication!',
            'Can\'t connect to server.',
            'error'
          );
        });
  }

}
