import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminEmailRoutes } from './admin-email.routing';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminEmailRoutes),
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: []
})

export class AdminEmailModule {}
