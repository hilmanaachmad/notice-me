import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes} from '@angular/router';
import { AddEmailComponent } from './add-email.component';
import { SharedModule } from '../../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';
import { EditorModule } from '@tinymce/tinymce-angular';
import { ReactiveFormsModule } from '@angular/forms';

export const AddEmailRoutes: Routes = [
  {
    path: '',
    component: AddEmailComponent,
    data: {
      breadcrumb: 'Add Email admin',
      status: true
    }
  }
];
 
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AddEmailRoutes),
    EditorModule,
    ReactiveFormsModule,
    SharedModule,
    NgxLoadingModule
  ],
  declarations: [AddEmailComponent]
})
export class AdminAddEmailModule { }
