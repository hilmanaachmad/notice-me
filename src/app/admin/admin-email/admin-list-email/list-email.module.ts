import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes} from '@angular/router';
import { ListEmailComponent } from './list-email.component';
import {FilterTablePipe} from './list-email.pipe'
import { SharedModule } from '../../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';
export const ListEmailRoutes: Routes = [
  {
    path: '',
    component: ListEmailComponent,
    data: {
      breadcrumb: 'List Email',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ListEmailRoutes),
    SharedModule,
    NgxLoadingModule
  ],
  declarations: [ListEmailComponent, FilterTablePipe]
})
export class AdminListEmailModule { }
