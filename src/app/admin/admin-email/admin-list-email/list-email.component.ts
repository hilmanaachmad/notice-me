import { Component, OnInit, Renderer, Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common'
import { HttpClient, HttpResponse } from '@angular/common/http';
import { AppService } from '../../../shared/service/app.service'
import swal from 'sweetalert2';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-email',
  templateUrl: './list-email.component.html',
  styleUrls: ['./list-email.component.css'],
  providers: [DatePipe]
})

export class ListEmailComponent implements OnInit {
  public data: any;
  public email_pengirim: any;
  public rowsOnPage: number = 10;
  public filterQuery: string = "";
  public sortBy: string = "Name";
  public sortOrder: string = "asc";
  public loading = false;

  private myDate = new Date();



  constructor(private router: Router, 
    public datepipe: DatePipe, 
    private http: HttpClient, 
    private service: AppService, 
    private renderer: Renderer) {

  }

  ngOnInit() {
    // this.email_pengirim = this.service.getEmail();
    this.loading = true;
    let data = ''
    // this.service.get('/api/noticeme/master-data/email/get' + '/' + this.email_pengirim, data)
    this.service.get('/api/noticeme/master-data/email/getEmailForAdmin', data)
      .subscribe(result => {
        this.loading = false;
        let valueResult = JSON.parse(result)
        if (valueResult.status == true) {
          this.data = valueResult.data
          console.log(this.data);
        } else {
          this.data = ''
        }
      }, error => {
        this.loading = false;
        swal.fire(
          'Authentication!',
          'Can\'t connect to server.',
          'error'
        );
      });
  }

  deleteEmail(id) {
    let data = ''
    swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      reverseButtons: true
    }).then(result => {
      if (result.value == true) {
        this.loading = true;
        this.service.delete('/api/noticeme/master-data/email/delete' + '/' + id, data)
          .subscribe(res => {
            this.loading = false;
            swal.fire("Good job!", "Delete Transaksi Email Success!", "success");
            this.ngOnInit();
          })
      }
    })
  }

  isApproved(id,isApproved){
    let data = ''
    swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Update it!',
      reverseButtons: true
    }).then(result => {
      if (result.value == true) {
        this.loading = true;
        this.service.put('/api/noticeme/master-data/email/isApproved' + '/' + id + '/' + isApproved, data)
          .subscribe(res => {
            this.loading = false;
            swal.fire("Good job!", "Success!", "success");
            this.ngOnInit();
          })
      }
    })
  }

  
  updateEmail(id){
    this.router.navigate(['admin/email/update-email'],{
      queryParams:{
        id: btoa(id)
      }
    })
  }
}
