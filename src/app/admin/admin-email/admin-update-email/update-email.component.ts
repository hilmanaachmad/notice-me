import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Http, Headers, Response } from "@angular/http";
import { Router, ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';
import { DatePipe } from '@angular/common';

import { AppService } from '../../../shared/service/app.service';
import { SessionService } from '../../../shared/service/session.service';

@Component({
  selector: 'app-update-email',
  templateUrl: './update-email.component.html',
  styleUrls: ['./update-email.component.css']
})
export class UpdateEmailComponent implements OnInit {
  public categories = [{ id: 'bydate', label: 'By date every month' }, { id: 'bybetween', label: 'By time period' }];
  tanggals = ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31'];
  public getemailpengirim: any;
  public data: any;
  private myDate = new Date();
  myForm: FormGroup;
  public loading = false;
  data_linesection = []
  datalevel = [
    {
      id: 'SUPERADMIN', name: 'SUPERADMIN'
    }
  ]
  id_tr: any

  constructor(
    private router: Router,
    private _http: Http,
    private http: HttpClient,
    private service: AppService,
    public session: SessionService,
    private datePipe: DatePipe,
    private activeRouter: ActivatedRoute
  ) {

    this.activeRouter.queryParams.subscribe(params => {
      this.id_tr = atob(params['id']);
    })

    let emailpengirim = new FormControl('', Validators.required);
    let emailpenerima = new FormControl('', Validators.required);
    let subject = new FormControl('', Validators.required);
    let pesan = new FormControl('', Validators.required);
    let start_date = new FormControl('', Validators.required);
    let end_date = new FormControl('', Validators.required);
    let by_date = new FormControl('', Validators.required);
    let isActived = new FormControl('', Validators.required);
    let kategori = new FormControl('', Validators.required)

    this.myForm = new FormGroup({
      emailpengirim: emailpengirim,
      emailpenerima: emailpenerima,
      subject: subject,
      pesan: pesan,
      start_date: start_date,
      end_date: end_date,
      by_date: by_date,
      isActived: isActived,
      kategori: kategori
    });


  }
  setKategoriValidators() {
    const by_date = this.myForm.get('by_date');
    const start_date = this.myForm.get('start_date');
    const end_date = this.myForm.get('end_date');

    let userCategory = this.myForm.value.kategori

    if (userCategory === 'bydate') {
      by_date.setValidators([Validators.required]);
      start_date.setValidators(null);
      end_date.setValidators(null);
      by_date.updateValueAndValidity();
      start_date.updateValueAndValidity();
      end_date.updateValueAndValidity();
      // this.myForm.controls['by_date'].setValue(null);
      this.myForm.controls['start_date'].setValue(null);
      this.myForm.controls['end_date'].setValue(null);
    }

    if (userCategory === 'bybetween') {
      by_date.setValidators(null);
      start_date.setValidators([Validators.required]);
      end_date.setValidators([Validators.required]);
      by_date.updateValueAndValidity();
      start_date.updateValueAndValidity();
      end_date.updateValueAndValidity();
      this.myForm.controls['by_date'].setValue(null);
      // this.myForm.controls['start_date'].setValue(null);
      // this.myForm.controls['end_date'].setValue(null);
    }



  }

  ngOnInit() {
    this.loading = true;
    let data = ''
    this.service.get('/api/noticeme/master-data/email/getdataemail' + '/' + this.id_tr, data)
      .subscribe(result => {
        this.loading = false;
        let valueResult = JSON.parse(result)
        if (valueResult.status == true) {
          this.data = valueResult.data
          this.buildForm(this.data);
          this.setKategoriValidators();
        } else {
          this.data = ''
        }
      }, error => {
        this.loading = false;
        swal.fire(
          'Authentication!',
          'Can\'t connect to server.',
          'error'
        );
      });
  }

  buildForm(data) {
    let id = new FormControl(data[0].id, Validators.required);
    let emailpengirim = new FormControl(data[0].email_pengirim, Validators.required);
    let emailpenerima = new FormControl(data[0].email_penerima, Validators.required);
    let subject = new FormControl(data[0].subject, Validators.required);
    let pesan = new FormControl(data[0].pesan, Validators.required);
    let start_date = new FormControl(data[0].start_date, Validators.required);
    let end_date = new FormControl(data[0].end_date, Validators.required);
    let by_date = new FormControl(data[0].by_date, Validators.required);
    let kategori = new FormControl(data[0].kategori, Validators.required);
    let isActived = new FormControl(data[0].isActived, Validators.required)

    this.myForm = new FormGroup({
      id: id,
      emailpengirim: emailpengirim,
      emailpenerima: emailpenerima,
      subject: subject,
      pesan: pesan,
      start_date: start_date,
      end_date: end_date,
      by_date: by_date,
      kategori: kategori,
      isActived: isActived
    });
  }

  submitted: boolean
  onSubmit() {

    this.submitted = true;
    if (this.myForm.valid) {
      let dataBody = this.myForm.value
      swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Update it!',
        reverseButtons: true
      }).then(result => {
        if (result.value == true) {
          this.loading = true;
          this.service.put('/api/noticeme/master-data/email/update', dataBody)
            .subscribe(data => {
              this.loading = false;
              let result = JSON.parse(data)
              if (result.status == true) {
                swal.fire("Good job!", "Update Transaksi Email Success!", "success");
                this.router.navigate(['admin/email/list-email']);
              } else {
                swal.fire(
                  'Authentication!',
                  'Failed Update.',
                  'error'
                );
              }
            }, error => {
              this.loading = false;
              swal.fire(
                'Authentication!',
                'Can\'t connect to server.',
                'error'
              );
            });
        }
      })
    }
  }
}
