import { Routes } from '@angular/router';

export const AdminEmailRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'add-email',
        loadChildren: './admin-add-email/add-email.module#AdminAddEmailModule',
        data: {
          breadcrumb: 'Add Email'
        }
      },
      {
        path: 'list-email',
        loadChildren: './admin-list-email/list-email.module#AdminListEmailModule',
        data: {
          breadcrumb: 'List Email'
        }
      },
      {
        path: 'update-email',
        loadChildren: './admin-update-email/update-email.module#AdminUpdateEmailModule',
        data: {
          breadcrumb : 'Update Email'
        }
      }
    ]
  }
];


