import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes} from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';
import { UpdateTelegramComponent } from './update-telegram.component';
export const UpdateTelegramRoutes: Routes = [
  {
    path: '',
    component: UpdateTelegramComponent,
    data: {
      breadcrumb: 'Update Telegram',
      status: true
    }
  }
];

@NgModule({
  declarations: [UpdateTelegramComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(UpdateTelegramRoutes),
    SharedModule,
    NgxLoadingModule
  ]
})
export class AdminUpdateTelegramModule { }
