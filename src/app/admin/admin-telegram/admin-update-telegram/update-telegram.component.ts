import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Http, Headers, Response } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';
import { DatePipe } from '@angular/common';
import { AppService } from '../../../shared/service/app.service';
import { SessionService } from '../../../shared/service/session.service';

@Component({
  selector: 'app-update-telegram',
  templateUrl: './update-telegram.component.html',
  styleUrls: ['./update-telegram.component.css']
})
export class UpdateTelegramComponent implements OnInit {
  public getpengirim: any;
  public data: any;
  private myDate = new Date();
  myForm: FormGroup;
  public loading = false;
  data_linesection = [];
  public id_tr :any;
  public options = [{ id: 1, label: 'Active' }, { id: 0, label: 'Not Active' }];
  public categories = [{ id: 'date', label: 'By date every month' }, { id: 'between', label: 'By time period' }];
  public dates = [
    { date: 1 }, { date: 2 }, { date: 3 }, { date: 4 },{ date: 5 },
    { date: 6 },{ date: 7 },{ date: 8 },{ date: 9 },{ date: 10 },
    { date: 11 },{ date: 12 },{ date: 13 },{ date: 14 },{ date: 15 },
    { date: 16 },{ date: 17 },{ date: 18 },{ date: 19 },{ date: 20 },
    { date: 21 },{ date: 22 },{ date: 23 },{ date: 24 },{ date: 25 },
    { date: 26 },{ date: 27 },{ date: 28 },{ date: 29 },{ date: 30 },{ date: 31 }];

  constructor(
    private router: Router,
    private _http: Http,
    private http: HttpClient,
    public service: AppService,
    public session: SessionService,
    private datePipe: DatePipe,
    private activeRouter: ActivatedRoute
    ) {

    this.activeRouter.queryParams.subscribe(params => {
      this.id_tr = atob(params['id']);
    })

    let nik = new FormControl('', Validators.required);
    let message = new FormControl('', Validators.required);
    let start_date = new FormControl('', Validators.required);
    let end_date = new FormControl('', Validators.required);
    let by_date = new FormControl('', Validators.required);
    let kategori = new FormControl('', Validators.required);
    let isActived = new FormControl('', Validators.required);

    this.myForm = new FormGroup({
      
      nik: nik,
      message: message,
      start_date: start_date,
      end_date: end_date,
      by_date: by_date,
      kategori: kategori,
      isActived: isActived
    });
  }

  setUserCategoryValidators() {
    const by_date = this.myForm.get('by_date');
    const start_date = this.myForm.get('start_date');
    const end_date = this.myForm.get('end_date');

    this.myForm.get('kategori').valueChanges
      .subscribe(kategori => {

        if (kategori === 'date') {
          by_date.setValidators([Validators.required]);
          start_date.setValidators(null);
          end_date.setValidators(null);
          this.myForm.controls['by_date'].setValue(null);
        }

        if (kategori === 'between') {
          by_date.setValidators(null);
          this.myForm.controls['start_date'].setValue('');
          this.myForm.controls['end_date'].setValue('');
          start_date.setValidators([Validators.required]);
          end_date.setValidators([Validators.required]);
        }

        by_date.updateValueAndValidity();
        start_date.updateValueAndValidity();
        end_date.updateValueAndValidity();
      });
  }

  ngOnInit() {

    this.loading = true;
    let data = '';
    this.service.get('/api/noticeme/master-data/telegram/getdatatelegram' + '/' + this.id_tr, data)
      .subscribe(result => {
        this.loading = false;
        let valueResult = JSON.parse(result)
        if (valueResult.status == true) {
          this.data = valueResult.data
          this.buildForm(this.data);
          this.setUserCategoryValidators();

        } else {
          this.data = '';
        }
      }, error => {
        this.loading = false;
        swal.fire(
          'Authentication!',
          'Can\'t connect to server.',
          'error'
        );
      });
  }

  buildForm(data){
    let id = new FormControl(data[0].id, Validators.required);
    let nik = new FormControl(data[0].nik, Validators.required);
    let message = new FormControl(data[0].message, Validators.required);
    let start_date = new FormControl(data[0].start_date, Validators.required);
    let end_date = new FormControl(data[0].end_date, Validators.required);
    let by_date = new FormControl(data[0].by_date, Validators.required);
    let kategori = new FormControl(data[0].kategori, Validators.required);
    let isActived = new FormControl(data[0].isActived, Validators.required);

    this.myForm = new FormGroup({
      id: id,
      nik: nik,
      message: message,
      start_date: start_date,
      end_date: end_date,
      by_date: by_date,
      kategori: kategori,
      isActived: isActived
    });
  }

  submitted: boolean
  onSubmit() {
    this.submitted = true;
    if (this.myForm.valid) {
      let dataBody = this.myForm.value
      swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Update it!',
        reverseButtons: true
      }).then(result => {
        if (result.value == true) {
          this.loading = true;
          this.service.put('/api/noticeme/master-data/telegram/update', dataBody)
            .subscribe(data => {
              this.loading = false;
              let result = JSON.parse(data)
              if (result.status == true) {
                swal.fire("Good job!", "Update Transaksi Telegram Success!", "success");
                this.router.navigate(['admin/telegram/list-telegram']);
              } else {
                swal.fire(
                  'Authentication!',
                  'Failed Update.',
                  'error'
                );
              }
            }, error => {
              this.loading = false;
              swal.fire(
                'Authentication!',
                'Can\'t connect to server.',
                'error'
              );
            });
        }
      });
    }
  }
}