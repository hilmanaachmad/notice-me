import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminTelegramRoutes } from './admin-telegram.routing';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminTelegramRoutes),
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: []
})

export class AdminTelegramModule {}
