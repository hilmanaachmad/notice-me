import { Routes } from '@angular/router';

export const AdminTelegramRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'add-telegram',
        loadChildren: './admin-add-telegram/add-telegram.module#AdminAddTelegramModule',
        data: {
          breadcrumb: 'Add telegram'
        }
      },
      {
        path: 'list-telegram',
        loadChildren: './admin-list-telegram/list-telegram.module#AdminListTelegramModule',
        data: {
          breadcrumb: 'List User'
        }
      },
      {
        path: 'update-telegram',
        loadChildren: './admin-update-telegram/update-telegram.module#AdminUpdateTelegramModule',
        data: {
          breadcrumb: 'Update telegram'
        }
      }
    ]
  }
];


