import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes} from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';
import { AddTelegramComponent } from './add-telegram.component';
export const AddTelegramRoutes: Routes = [
  {
    path: '',
    component: AddTelegramComponent,
    data: {
      breadcrumb: 'Add Telegram',
      status: true
    }
  }
];
@NgModule({
  declarations: [AddTelegramComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(AddTelegramRoutes),
    SharedModule,
    NgxLoadingModule
  ]
})
export class AdminAddTelegramModule { }
 