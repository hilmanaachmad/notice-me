import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Http, Headers, Response } from '@angular/http';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { DatePipe } from '@angular/common';

import { AppService } from '../../../shared/service/app.service';
import { SessionService } from '../../../shared/service/session.service';

@Component({
  selector: 'app-add-telegram',
  templateUrl: './add-telegram.component.html',
  styleUrls: ['./add-telegram.component.css'],
  providers: [DatePipe]
})
export class AddTelegramComponent implements OnInit {
  public nikPengirim: any;
  public UserInfo: any;
  public kondisi = true;
  private myDate = new Date();
  myForm: FormGroup;
  public loading = false;
  data_linesection = [];
  public options = [{ id: 1, label: 'Active' }, { id: 0, label: 'Not Active' }];
  public categories = [{ id: 'date', label: 'By date every month' }, { id: 'between', label: 'By time period' }];
  public dates = [
    { date: 1 }, { date: 2 }, { date: 3 }, { date: 4 },{ date: 5 },
    { date: 6 },{ date: 7 },{ date: 8 },{ date: 9 },{ date: 10 },
    { date: 11 },{ date: 12 },{ date: 13 },{ date: 14 },{ date: 15 },
    { date: 16 },{ date: 17 },{ date: 18 },{ date: 19 },{ date: 20 },
    { date: 21 },{ date: 22 },{ date: 23 },{ date: 24 },{ date: 25 },
    { date: 26 },{ date: 27 },{ date: 28 },{ date: 29 },{ date: 30 },{ date: 31 }];
  constructor(

    private router: Router,
    private _http: Http,
    private http: HttpClient,
    public service: AppService,
    public session: SessionService,
    private datePipe: DatePipe) {

    let nikPengirim = new FormControl ( this.service.getNik(), Validators.required);
    let nik = new FormControl('', Validators.required);
    let message = new FormControl('', Validators.required);
    let start_date = new FormControl('', Validators.required);
    let end_date = new FormControl('', Validators.required);
    let by_date = new FormControl(null, Validators.required);
    let kategori = new FormControl(null, Validators.required);
    let isActived = new FormControl(null, Validators.required);
  

    this.myForm = new FormGroup({
      nikPengirim: nikPengirim,
      nik: nik,
      message: message,
      start_date: start_date,
      end_date: end_date,
      by_date: by_date,
      kategori: kategori,
      isActived: isActived
    });


  }

  setUserCategoryValidators() {
    const by_date = this.myForm.get('by_date');
    const start_date = this.myForm.get('start_date');
    const end_date = this.myForm.get('end_date');

    this.myForm.get('kategori').valueChanges
      .subscribe(kategori => {

        if (kategori === 'date') {
          by_date.setValidators([Validators.required]);
          start_date.setValidators(null);
          end_date.setValidators(null);
        }

        if (kategori === 'between') {
          by_date.setValidators(null);
          start_date.setValidators([Validators.required]);
          end_date.setValidators([Validators.required]);
        }

        by_date.updateValueAndValidity();
        start_date.updateValueAndValidity();
        end_date.updateValueAndValidity();
      });
  }

  ngOnInit() {
    this.setUserCategoryValidators();
  }


  submitted: boolean
  onSubmit() {
    this.submitted = true;
    if (this.myForm.valid) {
      let dataBody = this.myForm.value
      swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, add it!',
        reverseButtons: true
      }).then(result => {
        if (result.value == true) {
          this.loading = true;
          this.service.post('/api/noticeme/master-data/telegram/add', dataBody)
            .subscribe(data => {
              this.loading = false;
              let result = JSON.parse(data)
              if (result.status == true) {
                swal.fire("Good job!", "Add User Success!", "success");
                this.myForm.reset();
              } else {
                swal.fire(
                  'Authentication!',
                  'Failed Insert.',
                  'error'
                );
              }
            }, error => {
              this.loading = false;
              swal.fire(
                'Authentication!',
                'Can\'t connect to server.',
                'error'
              );
            });
        }
      });
    }
  }

}