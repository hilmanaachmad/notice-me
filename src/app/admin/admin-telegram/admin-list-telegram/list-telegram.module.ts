import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import { ListTelegramComponent } from './list-telegram.component';
import {FilterTablePipe} from './list-telegram.pipe';
import {SharedModule} from '../../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

export const ListTelegramRoutes: Routes = [
  {
    path: '',
    component: ListTelegramComponent,
    data: {
      breadcrumb: 'List Telegram',
      status: true
    }
  }
];
@NgModule({
  declarations: [ListTelegramComponent, FilterTablePipe],
  imports: [
    CommonModule,
    RouterModule.forChild(ListTelegramRoutes),
    SharedModule,
    NgxLoadingModule
  ]
})
export class AdminListTelegramModule { }