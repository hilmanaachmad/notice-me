import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {ListUserComponent} from './list-user.component';
import {FilterTablePipe} from './list-user.pipe'
import {SharedModule} from '../../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';
export const ListUserRoutes: Routes = [
  {
    path: '',
    component: ListUserComponent,
    data: {
      breadcrumb: 'List User',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ListUserRoutes),
    SharedModule,
    NgxLoadingModule
  ],
  declarations: [ListUserComponent, FilterTablePipe]
})
export class ListUserModule { }
