import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes} from '@angular/router';
import { AddUserComponent } from './add-user.component';
import { SharedModule } from '../../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';
export const AddUserRoutes: Routes = [
  {
    path: '',
    component: AddUserComponent,
    data: {
      breadcrumb: 'Add User',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AddUserRoutes),
    SharedModule,
    NgxLoadingModule
  ],
  declarations: [AddUserComponent]
})
export class AddUserModule { }
