import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Http, Headers, Response } from "@angular/http";
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { DatePipe } from '@angular/common';

import { AppService } from '../../../shared/service/app.service';
import { SessionService } from '../../../shared/service/session.service';
@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css'],
  providers: [DatePipe]
})


export class AddUserComponent implements OnInit {
  private myDate = new Date();
  myForm: FormGroup;
  public loading = false;
  data_linesection = []
  datalevel = [
    {
      id: 'SUPERADMIN', name: 'SUPERADMIN'
    }
  ]

  constructor(
    private router: Router,
    private _http: Http,
    private http: HttpClient,
    private service: AppService,
    public session: SessionService,
    private datePipe: DatePipe) {

    let name = new FormControl('', Validators.required);
    let username = new FormControl('', Validators.required);


    this.myForm = new FormGroup({
      name: name,
      username: username
    });


  }

  ngOnInit() {

  }


  submitted: boolean
  onSubmit() {
    this.submitted = true;
    if (this.myForm.valid) {
      let dataBody = this.myForm.value
      swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, add it!',
        reverseButtons: true
      }).then(result => {
        if (result.value == true) {
          this.loading = true;
          this.service.post('/api/noticeme/master-data/user/add', dataBody)
            .subscribe(data => {
              this.loading = false;
              let result = JSON.parse(data)
              if (result.status == true) {
                swal.fire("Good job!", "Add User Success!", "success");
                this.myForm.reset()
              } else {
                swal.fire(
                  'Authentication!',
                  'Failed Insert.',
                  'error'
                );
              }
            }, error => {
              this.loading = false;
              swal.fire(
                'Authentication!',
                'Can\'t connect to server.',
                'error'
              );
            });
        }
      })
    }
  }
}