import { Routes } from '@angular/router';

export const UserRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'add-user',
        loadChildren: './add-user/add-user.module#AddUserModule',
        data: {
          breadcrumb: 'Add user'
        }
      },
      {
        path: 'list-user',
        loadChildren: './list-user/list-user.module#ListUserModule',
        data: {
          breadcrumb: 'List User'
        }
      },
      {
        path: 'update-user',
        loadChildren: './update-user/update-user.module#UpdateUserModule',
        data: {
          breadcrumb: 'Update User'
        }
      }
    ]
  }
];


