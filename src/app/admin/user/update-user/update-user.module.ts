import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes} from '@angular/router';
import { UpdateUserComponent } from './update-user.component';
import { SharedModule } from '../../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';
export const UpdateUserRoutes: Routes = [
  {
    path: '',
    component: UpdateUserComponent,
    data: {
      breadcrumb: 'Update User',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(UpdateUserRoutes),
    SharedModule,
    NgxLoadingModule
  ],
  declarations: [UpdateUserComponent]
})
export class UpdateUserModule { }
