import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Http, Headers, Response } from "@angular/http";
import { Router, ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';
import { DatePipe } from '@angular/common';

import { AppService } from '../../../shared/service/app.service';
import { SessionService } from '../../../shared/service/session.service';

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.css']
})
export class UpdateUserComponent implements OnInit {
private myDate = new Date();
  public data : any;
  myForm: FormGroup;
  public loading = false;
  data_linesection = []
  datalevel = [
    {
      id: 'SUPERADMIN', name: 'SUPERADMIN'
    }
  ]
  id_tr : any;

  constructor(
    private router: Router,
    private _http: Http,
    private http: HttpClient,
    private service: AppService,
    public session: SessionService,
    private datePipe: DatePipe,
    private activeRouter: ActivatedRoute
    ) {
    this.activeRouter.queryParams.subscribe(params => {
      this.id_tr = atob(params['id']);
    })

    let id = new FormControl('',Validators.required);
    let name = new FormControl('', Validators.required);
    let username = new FormControl('', Validators.required);


    this.myForm = new FormGroup({
      id: id,
      name: name,
      username: username
    });


  }

  ngOnInit() {
    this.loading = true;
    let data = ''
    this.service.get('/api/noticeme/master-data/user/getdatauser' + '/' + this.id_tr, data)
      .subscribe(result => {
        this.loading   = false;
        let valueResult = JSON.parse(result)
        if (valueResult.status == true) {
          this.data = valueResult.data
          this.buildForm(this.data)
        } else {
          this.data = ''
        }
      }, error => {
        this.loading = false;
        swal.fire(
          'Authentication!',
          'Can\'t connect to server.',
          'error'
        );
      });
  }
  buildForm(data){
    let id = new FormControl(data[0].id, Validators.required);
    let name = new FormControl(data[0].name, Validators.required);
    let username = new FormControl(data[0].username, Validators.required);

    this.myForm = new FormGroup({
      id: id,
      name: name,
      username: username
    });
  }

  submitted : boolean
  onSubmit(){
    this.submitted = true;
    if (this.myForm.valid) {
      let dataBody = this.myForm.value
      swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Update it!',
        reverseButtons: true
      }).then(result => {
        if (result.value == true) {
          this.loading = true;
          this.service.put('/api/noticeme/master-data/user/update', dataBody)
            .subscribe(data => {
              this.loading = false;
              let result = JSON.parse(data)
              if (result.status == true) {
                swal.fire("Good job!", "Update User Success!", "success");
                this.router.navigate(['admin/user/list-user']);
              } else {
                swal.fire(
                  'Authentication!',
                  'Failed Update.',
                  'error'
                );
              }
            }, error => {
              this.loading = false;
              swal.fire(
                'Authentication!',
                'Can\'t connect to server.',
                'error'
              );
            });
        }
      })
    }
  }
}
